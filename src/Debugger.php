<?php

namespace Baraja\BreakpointDebugger;

class Debugger
{

	/**
	 * @var Storage
	 */
	private static $storage;

	/**
	 * @var bool
	 */
	private static $logStop = false;

	public static function init(string $tempPath)
	{
		require_once __DIR__ . '/Storage.php';
		require_once __DIR__ . '/function.php';
		$tempPath = rtrim($tempPath, '/') . '/_barajaDebugger.txt';
		self::$storage = new Storage($tempPath);

		if (isset($_POST['debugWindowAjax'])) {
			if (isset($_POST['debugAction'])) {
				switch ($_POST['debugAction']) {
					case 'next':
						self::actionNext();
						break;
				}
				self::$storage->flush();
			}

			self::debugWindowAjax();
			die;
		}

		if (isset($_GET['debugWindow'])) {
			self::renderDebugWindow();
			die;
		}

		self::$storage->save('running', true);
		self::$storage->flush();
		self::$logStop = true;
	}

	public static function addBreakpoint(string $name = null, $data)
	{
		$backTrace = debug_backtrace();

		$haystack = [
			'running' => false,
			'breakpointName' => $name,
			'breakpointData' => $data,
			'file' => null,
			'line' => null,
			'callstack' => $backTrace,
		];

		foreach ($backTrace as $call) {
			if (isset($call['file']) && !preg_match('/baraja(\/|\\)[^\/\\]+(\/|\\).*$/', $call['file'])) {
				$haystack['file'] = $call['file'];
				$haystack['line'] = $call['line'];
				break;
			}
		}

		foreach ($haystack as $key => $value) {
			self::$storage->save($key, $value);
		}

		self::$storage->flush();

		while (true) {
			if ((bool) self::$storage->load('running', true) === true) {
				break;
			}

			usleep(1000);
		}
	}

	private static function renderDebugWindow()
	{
		$currentUrl = '//' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		echo str_replace('{currentUrl}', $currentUrl, file_get_contents(__DIR__ . '/window.html'));
	}

	private static function debugWindowAjax()
	{
		header('Content-Type: application/json');
		$callStack = '';

		foreach (self::$storage->load('callstack') as $call) {
			$callStack .= '<tr>';
			$callStack .= '<th>'
				. (isset($call->file) ? $call->file : 'Inner call')
				. '<span style="color:#aaa">' . (isset($call->line) ? '&nbsp;:&nbsp;' . $call->line : '') . '</span>'
				. (isset($call->file, $call->line) ? '<div class="code">' . self::renderFile($call->file, $call->line) . '</div>' : '')
				. '</th>';
			$callStack .= '</tr>';
		}

		$currentFile = self::$storage->load('file');
		$currentLine = self::$storage->load('line');

		echo json_encode(
			'<h1>' . self::$storage->load('breakpointName') . '</h1>'
			. ($currentFile && $currentLine
				? '<p><i>' . $currentFile . '</i> : ' . $currentLine . '</p>'
				. '<div class="code">' . self::renderFile($currentFile, $currentLine) . '</div>'
				: ''
			)
			. '<p>Last update: ' . date('d-m-Y H:i:s', filemtime(self::$storage->getTempFilePath()))
			. ' (' . filemtime(self::$storage->getTempFilePath()) . ')</p>'
			. '<h3 style="margin:1em 0">Callstack</h3>'
			. '<table class="table table-sm">' . $callStack . '</table>'
		);
	}

	private static function renderFile(string $file, int $line): string
	{
		if (!\is_file($file)) {
			return '';
		}

		$snippet = '';
		$lines = explode("\n", str_replace(["\r\n", "\r"], "\n", file_get_contents($file)));
		$startLine = $line - 8;
		$endLine = $line + 5;

		for ($_line = ($startLine < 1 ? 0 : $startLine); $_line <= $endLine; $_line++) {
			if (!isset($lines[$_line])) {
				break;
			}

			$snippet .= '<div class="code-line ' . ($_line + 1 === $line ? 'highlight' : '') . '">'
				. str_replace(' ', '&nbsp;', str_pad(($_line + 1) . ':', 6))
				. ($lines[$_line] ? str_replace("\t", '&nbsp;&nbsp;&nbsp;', $lines[$_line]) : '&nbsp;')
				. '</div>';
		}

		return $snippet;
	}

	private static function actionNext()
	{
		self::$storage->save('running', true);
		self::flashMessage('Continue...');
	}

	private static function flashMessage(string $message)
	{
		header('Content-Type: application/json');
		echo json_encode($message);
		self::$storage->flush();
		die;
	}

}