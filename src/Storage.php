<?php
/**
 * Created by PhpStorm.
 * User: janba
 * Date: 27.05.2018
 * Time: 16:25
 */

namespace Baraja\BreakpointDebugger;


class Storage
{

	/**
	 * @var string
	 */
	private $tempFile;

	/**
	 * @var string
	 */
	private $tempFilePath;

	/**
	 * @var int
	 */
	private $lastReloadTime;

	/**
	 * @var \stdClass[]
	 */
	private $data;

	public function __construct(string $tempFilePath)
	{
		$this->fixTempFile($tempFilePath);
		$this->tempFile = $tempFilePath;
		$this->data = json_decode(file_get_contents($tempFilePath));
		$this->tempFilePath = $tempFilePath;
		$this->lastReloadTime = time();
	}

	/**
	 * @param string $key
	 * @param null|mixed $value
	 */
	public function save(string $key, $value = null)
	{
		$this->data->{$key} = $value;
		$this->flush();
	}

	/**
	 * @param string $key
	 * @param bool $loadNow
	 * @return null|mixed
	 */
	public function load(string $key, bool $loadNow = false)
	{
		if ($loadNow === true || time() - $this->lastReloadTime >= 1) {
			$this->data = json_decode(file_get_contents($this->getTempFilePath()));
			$this->lastReloadTime = time();
		}

		if (isset($this->data->{$key})) {
			return $this->data->{$key};
		}

		return null;
	}

	public function flush()
	{
		$fp = fopen($this->tempFile, 'w+');
		if (flock($fp, LOCK_EX)) {
			fwrite($fp, json_encode($this->data));
			flock($fp, LOCK_UN);
		}
		fclose($fp);
	}

	public function toHtml(): string
	{
		$return = '';

		foreach ($this->data as $key => $value) {
			$return .= '<li><b>' . $key . '</b>: ' . json_encode($value) . '</li>';
		}

		return '<ul>' . $return . '</ul>';
	}

	/**
	 * @return string
	 */
	public function getTempFilePath(): string
	{
		return $this->tempFilePath;
	}

	/**
	 * @param string $path
	 */
	public function fixTempFile(string $path)
	{
		$default = [
			'active' => true,
			'running' => false,
			'fíle' => null,
			'line' => null,
			'breakpointName' => null,
			'callstack' => [],
			'stop' => false,
		];

		if (!is_file($path)) {
			file_put_contents($path, json_encode($default));
		} else {
			$fileContent = trim(file_get_contents($path));
			$file = json_decode($fileContent === '' ? '{}' : $fileContent, true);

			$allOk = true;
			foreach ($default as $key => $value) {
				if (!\in_array($key, array_keys($file), true)) {
					$allOk = false;
					break;
				}
			}

			if ($allOk === false) {
				file_put_contents($path, json_encode($default));
			}
		}
	}

}