<?php

function debug_breakpoint(string $name = null, $data = null)
{
	\Baraja\BreakpointDebugger\Debugger::addBreakpoint($name, $data);
}
