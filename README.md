Breakpoint debugger
===================

Runtime step-by-step code debugger in native PHP

This tool provides you a possibility to debug your PHP applications when being executed.

The debugger is similar to another existing tool: XDebug. However, our main advantage over other tools is it's quick installation without additional dependencies and simple usage even in production or CLI mode.

Installation
------------

The following instructions will guide you through the installation process.

1) Use Composer to install the dependency to your program.

```
composer require baraja/breakpoint-debugger
```

2) Initialize Debugger in index.php

```php
<?php

// Path to Debugger class
require __DIR__ . '/../vendor/baraja/breakpoint-debugger/src/Debugger.php';

// Path to a temporary directory
\Baraja\BreakpointDebugger\Debugger::init(__DIR__ . '/../temp');

// Optional: will extend the execution limit to 5 minutes
// set_time_limit(300);
```

Note: Debugger has to be loaded before Composer autoload in order to track every part of your application.

Usage
-----

To use Debugger just add parameter `?debugWindow` into the URL of your script. Sample URL: `http://localhost/project/index.php?debugWindow`.

If you want to add breakpoints into your application, add function call `debug_breakpoint(string $debug_message)`.

Example:

```php
function factorial(int $number) {
	if ($number < 2) {
		debug_breakpoint('Return 1, stop algorithm');
		return 1;
	} else {
		return ($number * factorial($number - 1));
	}
}
```

After starting your application in debug mode an additional window with debugging information will appear. The window will be updated automatically.

Each time PHP executes `debug_breakpoint` function the script will be suspended until the user decides what to do next.

